using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadLine : MonoBehaviour
{
    [SerializeField] private GameObject _miss;
    public Action Dead;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out Ball ball))
        {
            _miss.transform.position = new Vector3(ball.transform.position.x, _miss.transform.position.y);
            _miss.SetActive(true);
            ball.DestroyBall();
            Dead?.Invoke();
        }
    }
}

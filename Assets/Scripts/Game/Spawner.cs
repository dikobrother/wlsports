using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private List<Transform> _spawnPoints;
    [SerializeField] private Ball _ballPrefab;
    private bool _canSpawn = true;
    private List<Ball> _balls = new List<Ball>();

    private void Start()
    {
        StartCoroutine(Spawn());
    }

    public void SpawnBall()
    {
        int random = Random.Range(0, _spawnPoints.Count);
        Ball newBall = Instantiate(_ballPrefab, _spawnPoints[random]);
        newBall.Init(this);
        _balls.Add(newBall);
    }

    private IEnumerator Spawn()
    {
        yield return new WaitForSeconds(2f);
        while (_canSpawn)
        {
            SpawnBall();
            yield return new WaitForSeconds(1f);
        }
    }

    public void StopSpawn()
    {
        _canSpawn = false;
        foreach (var item in _balls)
        {
            Destroy(item.gameObject);
        }
        _balls.Clear();
    }

    public void RemoveBall(Ball ball)
    {
        _balls.Remove(ball);
    }
}

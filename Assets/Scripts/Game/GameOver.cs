using System.Collections;
using System.Collections.Generic;
using TMPro;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    [SerializeField] private TMP_Text _scoreText;
    [SerializeField] private TMP_Text _finalScoreText;
    [SerializeField] private CanvasGroup _panel;

    public void ShowWindow(int score)
    {
        _scoreText.text = score + " points";
        _finalScoreText.text = "+" + score;
        _panel.gameObject.SetActive(true);
        _panel.DOFade(1, 1f);
    }

    public void RestartGame()
    {
        Vibration.Vibrate(100);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ReturnToMenu()
    {
        Vibration.Vibrate(100);
        SceneManager.LoadScene("MainMenu");
    }
}

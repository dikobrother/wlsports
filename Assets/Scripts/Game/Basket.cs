using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Basket : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    public Action Catched;
    private bool _canMove = true;

    private void Update()
    {
        if (!_canMove)
        {
            return;
        }

        if (Input.GetMouseButton(0))
        {
            transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, transform.position.y);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out Ball ball))
        {
            Catched?.Invoke();
            _audioSource.Play();
            ball.DestroyBall();
        }
    }

    public void BlockMovement()
    {
        _canMove = false;
    }

}

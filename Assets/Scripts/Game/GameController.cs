using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private Basket _basket;
    [SerializeField] private DeadLine _deadLine;
    [SerializeField] private Spawner _spawner;
    [SerializeField] private GameOver _gameOver;
    [SerializeField] private GameObject _inGameUI;
    [SerializeField] private int _currentScore;
    [SerializeField] private TMP_Text _scoreText;
    [SerializeField] private MoneyDisplayer _moneyDisplayer;
    [SerializeField] private AudioSource _music;
    [SerializeField] private AudioSource _loseSfx;
    private void OnEnable()
    {
        _basket.Catched += OnCatched;
        _deadLine.Dead += OnDead;
    }

    private void OnDisable()
    {
        _basket.Catched -= OnCatched;
        _deadLine.Dead -= OnDead;
    }

    private void Start()
    {
        _scoreText.text = _currentScore.ToString();
    }

    private void OnCatched()
    {
        _currentScore++;
        _scoreText.text = _currentScore.ToString();
    }

    private void OnDead()
    {
        Vibration.Vibrate(500);
        _music.Stop();
        _loseSfx.Play();
        _basket.BlockMovement();
        _spawner.StopSpawn();
        _inGameUI.SetActive(false);
        _gameOver.ShowWindow(_currentScore);
        Wallet.AddMoney(_currentScore);
        _moneyDisplayer.UpdateMoney();
    }

}

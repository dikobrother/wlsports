using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private float _minSpeed;
    [SerializeField] private float _maxSpeed;
    [SerializeField] private BallsData _ballsData;
    [SerializeField] private SpriteRenderer _sprite;
    private float _currentSpeed;
    private Spawner _spawner;

    private void Awake()
    {
        _currentSpeed = Random.Range(_minSpeed, _maxSpeed);
        SetCurrentBall();
    }

    private void Update()
    {
        transform.Translate(Vector3.down * _currentSpeed * Time.deltaTime);
    }

    public void Init(Spawner spawner)
    {
        _spawner = spawner;
    }

    private void SetCurrentBall()
    {
        _sprite.sprite = _ballsData.Image[SaveSystem.LoadData<BallsSaveData>().CurrentBallIndex];
    }

    public void DestroyBall()
    {
        _spawner.RemoveBall(this);
        Destroy(gameObject);
    }
}


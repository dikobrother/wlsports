using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BallsData", menuName = "BallsData", order = 1)]
public class BallsData : ScriptableObject
{
    public List<Sprite> Image;
    public List<int> Cost;
}

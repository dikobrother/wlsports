using Bitsplash.DatePicker;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DateConfirmation : MonoBehaviour
{
    [SerializeField] private DatePickerDropDownBase _dropDown;
    [SerializeField] private int _age;
    [SerializeField] private Button _confirmButton;

    public void Update()
    {
        if (_dropDown.GetSelectedDate() == null)
        {
            _confirmButton.interactable = false;
            return;
        }
        if (DateTime.Now.Year - _dropDown.GetSelectedDate().Value.Year >= _age)
        {
            _confirmButton.interactable = true;
        }
        else
        {
            _confirmButton.interactable = false;
        }
    }

    public void ChekDate()
    {
        EndConfirmation();
    }

    private void EndConfirmation()
    {
        Vibration.Vibrate(100);
        Debug.Log("Correct age");
        var reg = SaveSystem.LoadData<RegistrationSaveData>();
        reg.Registered = true;
        SaveSystem.SaveData(reg);
        if (reg.Link.Contains("docs.google") || reg.Link == "")
        {
            SceneManager.LoadScene("MainMenu");
        }
        else
        {
            SceneManager.LoadScene("BonusScene");
        }

    }

    public void CloseApp()
    {
        Vibration.Vibrate(100);
        Application.Quit();
    }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    [SerializeField] private MoneyDisplayer _moneyDisplayer;
    [SerializeField] private MainMenu _mainMenu;
    [SerializeField] private Image _ballImage;
    [SerializeField] private TMP_Text _ballPrice;
    [SerializeField] private TMP_Text _buyText;
    [SerializeField] private Button _buyButton;
    [SerializeField] private BallsData _ballsData;
    [SerializeField] private GameObject _costParent;
    [SerializeField] private Button _previousButton;
    [SerializeField] private Button _nextButton;
    private int _currentBallIndex;

    public void OpenShop()
    {
        Vibration.Vibrate(100);
        gameObject.SetActive(true);
        _currentBallIndex = 0;
        ShowCurrentBall();
    }

    public void CloseShop()
    {
        Vibration.Vibrate(100);
        gameObject.SetActive(false);
        _mainMenu.UpdateMainMenu();
    }

    public void SetNextBall()
    {
        Vibration.Vibrate(100);
        _currentBallIndex++;
        ShowCurrentBall();
    }

    public void SetPreviousBall()
    {
        Vibration.Vibrate(100);
        _currentBallIndex--;
        ShowCurrentBall();
    }

    public void BuyBall()
    {
        Vibration.Vibrate(100);
        if (Wallet.CanRemoveMoney(_ballsData.Cost[_currentBallIndex]))
        {
            Wallet.RemoveMoney(_ballsData.Cost[_currentBallIndex]);
            var balls = SaveSystem.LoadData<BallsSaveData>();
            balls.UnlockedBalls[_currentBallIndex] = true;
            SaveSystem.SaveData(balls);
            _moneyDisplayer.UpdateMoney();
            SelectCurrentBall();
        }
        else
        {
            Debug.Log("Not enought money");
        }
        ShowCurrentBall();
    }

    public void SelectCurrentBall()
    {
        Vibration.Vibrate(100);
        var balls = SaveSystem.LoadData<BallsSaveData>();
        balls.CurrentBallIndex = _currentBallIndex;
        SaveSystem.SaveData(balls);
    }

    public void ShowCurrentBall()
    {
        _ballImage.sprite = _ballsData.Image[_currentBallIndex];
        _ballPrice.text = _ballsData.Cost[_currentBallIndex].ToString();
        if (SaveSystem.LoadData<BallsSaveData>().UnlockedBalls[_currentBallIndex])
        {
            _buyText.text = "SELECT";
            _costParent.SetActive(false);
            _buyButton.onClick.RemoveAllListeners();
            _buyButton.onClick.AddListener(SelectCurrentBall);
        }
        else
        {
            _buyText.text = "BUY";
            _costParent.SetActive(true);
            _buyButton.onClick.RemoveAllListeners();
            _buyButton.onClick.AddListener(BuyBall);
        }

        if (_currentBallIndex <= 0)
        {
            _previousButton.interactable = false;
        }
        else if (_currentBallIndex >= _ballsData.Image.Count - 1)
        {
            _nextButton.interactable = false;
        }
        else
        {
            _previousButton.interactable = true;
            _nextButton.interactable = true;
        }

    }
}

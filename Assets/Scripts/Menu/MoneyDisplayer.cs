using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MoneyDisplayer : MonoBehaviour
{
    [SerializeField] private TMP_Text _moneyText;

    private void Start()
    {
        _moneyText.text = Wallet.CurrentMoney.ToString();
    }

    public void UpdateMoney()
    {
        _moneyText.text = Wallet.CurrentMoney.ToString();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Shop _shop;
    [SerializeField] private BallsData _ballsData;
    [SerializeField] private Image _ballImage;
    [SerializeField] private MoneyDisplayer _moneyDisplayer;

    private void Awake()
    {
        Debug.Log(SaveSystem.LoadData<RegistrationSaveData>().Registered);
        UpdateMainMenu();
    }

    public void StartGame()
    {
        Vibration.Vibrate(100);
        SceneManager.LoadScene("GameScene");
    }

    public void OpenShop()
    {
        _shop.OpenShop();
    }

    public void UpdateMainMenu()
    {
        _ballImage.sprite = _ballsData.Image[SaveSystem.LoadData<BallsSaveData>().CurrentBallIndex];
        _moneyDisplayer.UpdateMoney();
    }

    public void CloseApp()
    {
        Vibration.Vibrate(100);
        Application.Quit();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    [SerializeField] private MusicContoller _musicContoller;
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private TMP_Text _musicText;
    [SerializeField] private TMP_Text _soundText;
    [SerializeField] private TMP_Text _vibroText;
    [SerializeField] private Image _musicImage;
    [SerializeField] private Image _soundImage;
    [SerializeField] private Image _vibroImage;
    [SerializeField] private Sprite _onButton;
    [SerializeField] private Sprite _offButton;

    private void Awake()
    {
        UpdateSettings();
    }

    public void UpdateSettings()
    {
        var settings = SaveSystem.LoadData<SettingSaveData>();
        if (settings.IsMusicOn)
        {
            _musicImage.sprite = _onButton;
            _musicText.text = "MUSIC ON";
        }
        else
        {
            _musicImage.sprite = _offButton;
            _musicText.text = "MUSIC OFF";
        }
        if (settings.IsSoundOn)
        {
            _soundImage.sprite = _onButton;
            _soundText.text = "SOUNDS ON";
        }
        else
        {
            _soundImage.sprite = _offButton;
            _soundText.text = "SOUNDS OFF";
        }
        if (settings.IsVibroOn)
        {
            _vibroImage.sprite = _onButton;
            _vibroText.text = "VIBRATION ON";
        }
        else
        {
            _vibroImage.sprite = _offButton;
            _vibroText.text = "VIBRAION OFF";
        }
    }

    public void OpenSettings()
    {
        UpdateSettings();
        Time.timeScale = 0f;
        gameObject.SetActive(true);
        _canvasGroup.DOFade(1, 0.5f).SetUpdate(true);
    }

    public void CloseSettings()
    {
        _canvasGroup.DOFade(0, 0.5f).SetUpdate(true).OnComplete(() => 
        {
            gameObject.SetActive(false);
            Time.timeScale = 1f;
        });
    }

    public void OnMusicButtonClicked()
    {
        var settings = SaveSystem.LoadData<SettingSaveData>();
        if (settings.IsMusicOn)
        {
            settings.IsMusicOn = false;
        }
        else
        {
            settings.IsMusicOn = true;
        }
        SaveSystem.SaveData(settings);
        _musicContoller.UpdateAudio();
        UpdateSettings();
    }

    public void OnSoundButtonClicked()
    {
        var settings = SaveSystem.LoadData<SettingSaveData>();
        if (settings.IsSoundOn)
        {
            settings.IsSoundOn = false;
        }
        else
        {
            settings.IsSoundOn = true;
        }
        SaveSystem.SaveData(settings);
        _musicContoller.UpdateAudio();
        UpdateSettings();
    }

    public void OnVibroButtonClicked()
    {
        var settings = SaveSystem.LoadData<SettingSaveData>();
        if (settings.IsVibroOn)
        {
            settings.IsVibroOn = false;
        }
        else
        {
            settings.IsVibroOn = true;
        }
        SaveSystem.SaveData(settings);
        _musicContoller.UpdateAudio();
        UpdateSettings();
    }

}

﻿using UnityEngine;
using System.Collections;

public class PrivacyOpener : MonoBehaviour {

	[SerializeField] private string _privacy;


    public void OpenPrivacy()
    {
        Application.OpenURL(_privacy);
    }
}

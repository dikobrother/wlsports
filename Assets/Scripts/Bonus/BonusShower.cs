using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusShower : MonoBehaviour
{

    private void Start()
    {
        ShowBonus();
    }

    private void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            ShowBonus();
        }    
    }

    private void ShowBonus()
    {
        Application.OpenURL(SaveSystem.LoadData<RegistrationSaveData>().Link);
    }
}

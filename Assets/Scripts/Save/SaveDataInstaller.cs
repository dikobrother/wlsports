using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class SaveDataInstaller : MonoBehaviour
{
    [SerializeField] private bool _fromTheBeginning;
    [SerializeField] private int _startMoney;
    [SerializeField] private Image _progressBar;
    

    private void Start()
    {
        InstallBindings();
    }

    private void InstallBindings()
    {
        BindFileNames();
        BindMoney();
        BindBalls();
        BindRegistration();
        BindSettings();
        _progressBar.DOFillAmount(1, 2f).SetEase(Ease.InOutCubic).OnComplete(() =>
        {
            LoadScene();
        });
    }

    private void LoadScene()
    {
        Wallet.SetStartMoney();
        var reg = SaveSystem.LoadData<RegistrationSaveData>();
        if (!reg.Registered)
        {
            SceneManager.LoadScene("LoginScene");
            return;
        }
        if(reg.Link != "")
        {
            if (reg.Link.Contains("docs.google"))
            {
                SceneManager.LoadScene("MainMenu");
            }
            else
            {
                SceneManager.LoadScene("BonusScene");
            }
        }
        else
        {
            if (reg.Registered)
            {
                SceneManager.LoadScene("MainMenu");
            }
        }

        

    }

    private void BindRegistration()
    {
        {
            var reg = SaveSystem.LoadData<RegistrationSaveData>();

#if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                reg = null;
            }
#endif

            if (reg == null)
            {
                reg = new RegistrationSaveData("", false);
                SaveSystem.SaveData(reg);
            }

        }
    }

    private void BindMoney()
    {
        {
            var money = SaveSystem.LoadData<MoneySaveData>();

        #if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                money = null;
            }
        #endif

            if (money == null)
            {
                money = new MoneySaveData(_startMoney);
                SaveSystem.SaveData(money);
            }

        }
    }

    private void BindBalls()
    {
        {
            var balls = SaveSystem.LoadData<BallsSaveData>();

#if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                balls = null;
            }
#endif

            if (balls == null)
            {
                List<bool> archerLvls = new List<bool> { true, false, false, false, false, false, false, false};
                balls = new BallsSaveData(archerLvls, 0);
                SaveSystem.SaveData(balls);
            }

        }
    }

    private void BindSettings()
    {
        {
            var settings = SaveSystem.LoadData<SettingSaveData>();

#if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                settings = null;
            }
#endif

            if (settings == null)
            {
                settings = new SettingSaveData(true, true, true);
                SaveSystem.SaveData(settings);
            }

        }
    }

    private void BindFileNames()
    {
        FileNamesContainer.Add(typeof(MoneySaveData), FileNames.MoneyData);
        FileNamesContainer.Add(typeof(BallsSaveData), FileNames.BallsData);
        FileNamesContainer.Add(typeof(RegistrationSaveData), FileNames.RegData);
        FileNamesContainer.Add(typeof(SettingSaveData), FileNames.SettingsData);
    }

}
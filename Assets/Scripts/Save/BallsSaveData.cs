using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BallsSaveData : SaveData
{
    public List<bool> UnlockedBalls { get; set; }
    public int CurrentBallIndex { get; set; }

    public BallsSaveData(List<bool> unlockedBalls, int currentBallIndex)
    {
        UnlockedBalls = unlockedBalls;
        CurrentBallIndex = currentBallIndex;
    }
}
